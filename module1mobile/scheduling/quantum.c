// author: Reinhart, Jonathon, Fall 2010

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>

int main(int argc, char** argv) {
   struct timespec t;

   int n=4;
   if (argc > 1)
      n = atoi(argv[1]);

   pid_t child;
   int i;
   for (i=0; i < n-1; i++) {
      child = fork();
      if (child == -1) {
         perror("Could not fork");
         exit(2);
      }
      if (child == 0)
         break;
   }

   int q, last_q=0;
   char dir;
   while (1) {
      if (sched_rr_get_interval(getpid(), &t)) {
         perror("ERROR");
         exit(1);
      }
      q = t.tv_nsec/1000;
      dir = (q==last_q) ? '=' : ((q<last_q) ? 'v' : '^');
      last_q = q;

      fprintf(stderr, "process %3d) quantum: %6d us   %c\n",
         i, q, dir);

      sleep(1);
   }
}
