package main

import (
   "os"
   "time"
   "fmt"
   "strconv"
   "sync"
)

// multiple producers and multiple consumers, each only trying one

var wg = &sync.WaitGroup{}

func producer(buf chan<- string, i int, pturn chan int) {
    turn := <- pturn
    if turn == i {
       timestr := time.Now().Local().Format("15:04:05.000")
       buf <- timestr
       fmt.Printf("Producer %d just put %s in the shared buffer.\n", i, timestr)
       pturn <- turn+1
    } else {
         pturn <- turn
         producer(buf,i,pturn)
      }
    wg.Done()
 
}

func consumer(buf <-chan string, i int, cturn chan int) {
    turn := <- cturn
    if turn == i {
       item := <- buf
       fmt.Printf("Consumer %d just consumed %s from the shared buffer.\n", i, item)
       cturn <- turn+1
    } else {
         cturn <- turn
         consumer(buf,i,cturn)
      }
    wg.Done()
}

func main() {

   length,_ := strconv.Atoi(os.Args[1]) // like atoi() function in C
   num_ps,_ := strconv.Atoi(os.Args[2]) // like atoi() function in C
   num_cs,_ := strconv.Atoi(os.Args[3]) // like atoi() function in C

   buffer := make(chan string, length)

   pturn := make(chan int)
   cturn := make(chan int)

   for i:=0; i<num_ps; i++ {
     go producer(buffer,i,pturn)
   }

   pturn <- 0

   for i:=0; i<num_cs; i++ {
      go consumer(buffer,i,cturn)
   }

   cturn <- 0

   wg.Add(num_ps+num_cs)
   wg.Wait()
}
